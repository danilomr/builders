package br.com.builders.customers.service;

import br.com.builders.customers.entity.Customer;
import br.com.builders.customers.exception.CustomerAlreadyExistsException;
import br.com.builders.customers.exception.CustomerNotFoundException;
import br.com.builders.customers.repository.CustomerRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Page<Customer> findByNameAndCpf(final String name,
                                           final String cpf,
                                           final int page,
                                           final int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        if(StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(cpf)) {
            return customerRepository.findByNameIgnoreCaseContainingAndCpf(name, cpf, pageRequest);

        }else if(StringUtils.isNotEmpty(name) && StringUtils.isEmpty(cpf)) {
            return customerRepository.findByNameIgnoreCaseContaining(name, pageRequest);

        }else if(StringUtils.isEmpty(name) && StringUtils.isNotEmpty(cpf)) {
            return customerRepository.findByCpf(cpf, pageRequest);
        }
        return new PageImpl<>(new ArrayList<>());
    }

    public boolean existsByCpf(final String cpf) {
        return customerRepository.existsByCpf(cpf);
    }

    public Customer save(final Customer customer) {
        if(existsByCpf(customer.getCpf())) {
            throw new CustomerAlreadyExistsException(customer.getCpf());
        }
        return customerRepository.save(customer);
    }

    public Customer update(final Long customerId, final Customer customer) {
        customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
        return customerRepository.save(customer.toBuilder().id(customerId).build());
    }

    public void delete(final Long customerId) {
        customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
        customerRepository.deleteById(customerId);
    }

    public Customer patch(final Long customerId, final Customer customer) {
        Customer existingCustomer = customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
        Optional.ofNullable(customer.getName()).ifPresent(existingCustomer::setName);
        Optional.ofNullable(customer.getCpf()).ifPresent(existingCustomer::setCpf);
        Optional.ofNullable(customer.getBirthdate()).ifPresent(existingCustomer::setBirthdate);
        return customerRepository.save(existingCustomer);
    }
}
