package br.com.builders.customers.controller.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class DefaultErrorResponseDTO {
    private int status;
    private String message;
}
