package br.com.builders.customers.exception;

public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException(final Long customerId) {
        super(String.format("Customer not found with id [%d]", customerId));
    }
}
