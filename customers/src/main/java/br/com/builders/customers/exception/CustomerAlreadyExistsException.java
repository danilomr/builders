package br.com.builders.customers.exception;

public class CustomerAlreadyExistsException extends RuntimeException {
    public CustomerAlreadyExistsException(final String cpf) {
        super(String.format("Customer already exists with CPF [%s]", cpf));
    }
}
