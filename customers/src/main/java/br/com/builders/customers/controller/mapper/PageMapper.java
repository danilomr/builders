package br.com.builders.customers.controller.mapper;

import br.com.builders.customers.controller.dto.PageDTO;
import br.com.builders.customers.entity.Customer;
import org.springframework.data.domain.Page;

public class PageMapper {

    public static PageDTO toDTO(final Page<Customer> customerPage) {
        return PageDTO.builder()
                .page(customerPage.getNumber())
                .size(customerPage.getSize())
                .totalPages(customerPage.getTotalPages())
                .totalElements(Math.toIntExact(customerPage.getTotalElements()))
                .build();
    }
}
