package br.com.builders.customers.repository;

import br.com.builders.customers.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Page<Customer> findByNameIgnoreCaseContaining(final String name, Pageable pageable);

    Page<Customer> findByCpf(final String cpf, Pageable pageable);

    Page<Customer> findByNameIgnoreCaseContainingAndCpf(final String name, final String cpf, Pageable pageable);

    Optional<Customer> findById(final Long id);

    boolean existsByCpf(final String cpf);
}
