package br.com.builders.customers.controller.mapper;

import br.com.builders.customers.controller.dto.*;
import br.com.builders.customers.entity.Customer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerMapper {

    public static GetCustomerDTO toDTOPage(final Page<Customer> customerPage) {
        List<CustomerDTO> content = toDTOList(customerPage.getContent());
        return GetCustomerDTO.builder()
                .content(content)
                .page(PageMapper.toDTO(customerPage))
                .build();
    }

    public static List<CustomerDTO> toDTOList(final List<Customer> customerList) {
        return customerList.stream()
                .map(customer -> toDTO(customer))
                .collect(Collectors.toList());
    }

    public static CustomerDTO toDTO(final Customer customer) {
        return CustomerDTO.builder()
                .id(customer.getId())
                .name(customer.getName())
                .cpf(customer.getCpf())
                .age(customer.getAge())
                .birthdate(DateTimeFormatter.ofPattern("dd-MM-yyyy").format(customer.getBirthdate()))
                .build();
    }

    public static Customer toEntity(final CreateCustomerDTO createCustomerDTO) {
        return Customer.builder()
                .name(createCustomerDTO.getName())
                .cpf(createCustomerDTO.getCpf())
                .birthdate(LocalDate.parse(createCustomerDTO.getBirthdate(), DateTimeFormatter.ofPattern("dd-MM-yyyy")))
                .build();

    }

    public static Customer toEntity(final UpdateCustomerDTO updateCustomerDTO) {
        return Customer.builder()
                .name(updateCustomerDTO.getName())
                .cpf(updateCustomerDTO.getCpf())
                .birthdate(LocalDate.parse(updateCustomerDTO.getBirthdate(), DateTimeFormatter.ofPattern("dd-MM-yyyy")))
                .build();

    }

    public static Customer toEntity(final PatchCustomerDTO patchCustomerDTO) {
        return Customer.builder()
                .name(StringUtils.isEmpty(patchCustomerDTO.getName()) ? null : patchCustomerDTO.getName())
                .cpf(StringUtils.isEmpty(patchCustomerDTO.getCpf()) ? null : patchCustomerDTO.getCpf())
                .birthdate(StringUtils.isEmpty(patchCustomerDTO.getBirthdate()) ? null :
                        LocalDate.parse(patchCustomerDTO.getBirthdate(), DateTimeFormatter.ofPattern("dd-MM-yyyy")))
                .build();
    }
}
