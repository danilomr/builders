package br.com.builders.customers.controller;
import br.com.builders.customers.controller.dto.DefaultErrorResponseDTO;
import br.com.builders.customers.exception.CustomerAlreadyExistsException;
import br.com.builders.customers.exception.CustomerNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.format.DateTimeParseException;

@ControllerAdvice
public class CustomerControllerAdvice {

    private static final String INVALID_DATE_MESSAGE = "Field [birthdate] is invalid. The correct format is (dd-MM-yyyy).";

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    protected ResponseEntity<DefaultErrorResponseDTO> handle(final MethodArgumentNotValidException ex) {
        return buildResponse(HttpStatus.BAD_REQUEST, ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }

    @ExceptionHandler(value = CustomerAlreadyExistsException.class)
    protected ResponseEntity<DefaultErrorResponseDTO> handle(final CustomerAlreadyExistsException ex) {
        return buildResponse(HttpStatus.CONFLICT, ex.getMessage());
    }

    @ExceptionHandler(value = CustomerNotFoundException.class)
    protected ResponseEntity<DefaultErrorResponseDTO> handle(final CustomerNotFoundException ex) {
        return buildResponse(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(value = DateTimeParseException.class)
    protected ResponseEntity<DefaultErrorResponseDTO> handle(final DateTimeParseException ex) {
        return buildResponse(HttpStatus.BAD_REQUEST, INVALID_DATE_MESSAGE);
    }

    private ResponseEntity<DefaultErrorResponseDTO> buildResponse(final HttpStatus status, final String message) {
        return ResponseEntity.status(status)
                .body(DefaultErrorResponseDTO.builder()
                    .status(status.value())
                    .message(message)
                    .build());
    }
}
