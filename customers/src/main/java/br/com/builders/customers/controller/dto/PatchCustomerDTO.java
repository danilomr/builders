package br.com.builders.customers.controller.dto;

import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class PatchCustomerDTO {

    private String name;

    @Pattern(regexp="[\\d]{11}", message = "CPF must contain only numbers and have maximum length of 11 chars")
    private String cpf;
    private String birthdate;
}
