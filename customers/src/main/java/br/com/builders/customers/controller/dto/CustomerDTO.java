package br.com.builders.customers.controller.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CustomerDTO {
    private Long id;
    private String name;
    private String cpf;
    private String birthdate;
    private Integer age;
}
