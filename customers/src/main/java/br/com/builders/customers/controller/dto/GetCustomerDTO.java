package br.com.builders.customers.controller.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class GetCustomerDTO {

    List<CustomerDTO> content;
    PageDTO page;
}
