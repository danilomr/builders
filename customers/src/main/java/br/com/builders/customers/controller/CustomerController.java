package br.com.builders.customers.controller;

import br.com.builders.customers.controller.dto.*;
import br.com.builders.customers.controller.mapper.CustomerMapper;
import br.com.builders.customers.entity.Customer;
import br.com.builders.customers.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping(path = "/customer")
@RestController
@Validated
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity<GetCustomerDTO> getCustomer(@RequestParam(value = "name", required = false) String name,
                                                      @RequestParam(value = "cpf", required = false) String cpf,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "size") int size) {

        GetCustomerDTO pageableCustomers = CustomerMapper.toDTOPage(customerService
                .findByNameAndCpf(name, cpf, page, size));
        return ResponseEntity.ok(pageableCustomers);
    }

    @PostMapping
    public ResponseEntity<CustomerDTO> createCustomer(@Valid @RequestBody CreateCustomerDTO createCustomerDTO) {
        Customer customer = customerService.save(CustomerMapper.toEntity(createCustomerDTO));
        return ResponseEntity.ok(CustomerMapper.toDTO(customer));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<CustomerDTO> updateCustomer(@PathVariable("id") Long customerId,
                                                        @Valid @RequestBody UpdateCustomerDTO updateCustomerDTO) {
        Customer customer = customerService.update(customerId, CustomerMapper.toEntity(updateCustomerDTO));
        return ResponseEntity.ok(CustomerMapper.toDTO(customer));
    }

    @PatchMapping(path = "/{id}")
    public ResponseEntity<CustomerDTO> patchCustomer(@PathVariable("id") Long customerId,
                                                     @Valid @RequestBody PatchCustomerDTO patchCustomerDTO) {
        Customer customer = customerService.patch(customerId, CustomerMapper.toEntity(patchCustomerDTO));
        return ResponseEntity.ok(CustomerMapper.toDTO(customer));
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable("id") Long customerId) {
        customerService.delete(customerId);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
