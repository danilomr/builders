package br.com.builders.customers.controller.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class PageDTO {
    private int page;
    private int size;
    private int totalPages;
    private int totalElements;
}
