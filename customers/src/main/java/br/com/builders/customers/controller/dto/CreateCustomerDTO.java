package br.com.builders.customers.controller.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class CreateCustomerDTO {

    @NotEmpty(message = "The field 'Name' cannot be null")
    private String name;

    @NotEmpty(message = "The field 'CPF' cannot be null")
    @Pattern(regexp = "[\\d]{11}", message = "CPF must contain only numbers and have maximum length of 11 chars")
    private String cpf;

    @NotEmpty(message = "The field 'Birthdate' cannot be null")
    private String birthdate;
}
