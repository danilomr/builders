package br.com.builders.customers.fixture;

import br.com.builders.customers.controller.dto.PatchCustomerDTO;

public class PatchCustomerFixture {

    public static PatchCustomerDTO defaultValues() {
        return PatchCustomerDTO.builder()
                .name("Customer 1")
                .cpf("11122233345")
                .birthdate("10-10-2010")
                .build();
    }
}
