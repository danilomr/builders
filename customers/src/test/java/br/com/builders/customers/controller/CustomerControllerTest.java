package br.com.builders.customers.controller;

import br.com.builders.customers.entity.Customer;
import br.com.builders.customers.fixture.CustomerFixture;
import br.com.builders.customers.service.CustomerService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.StreamUtils;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerController customerController;

    @MockBean
    private CustomerService customerService;

    @Test
    public void shouldGetCustomers() throws Exception {
        when(customerService.findByNameAndCpf(anyString(), anyString(), anyInt(), anyInt()))
                .thenReturn(CustomerFixture.defaultPage());

        mockMvc.perform(MockMvcRequestBuilders
                .get("/customer?name=Test&cpf=11122233345&page=0&size=10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").exists())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.page").exists());

        verify(customerService, times(1))
                .findByNameAndCpf(anyString(), anyString(), anyInt(), anyInt());
    }

    @Test
    public void shouldCreateCustomer() throws Exception {
        when(customerService.save(any(Customer.class))).thenReturn(CustomerFixture.defaultValues());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("create-customer.json")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Customer 1"))
                .andExpect(jsonPath("$.cpf").value("11122233345"))
                .andExpect(jsonPath("$.birthdate").value("10-12-1990"))
                .andExpect(jsonPath("$.age").exists());

        verify(customerService, times(1)).save(any(Customer.class));
    }

    @Test
    public void shouldNotCreateCustomerWhenNameIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("create-customer-null-name.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'Name' cannot be null"));

        verify(customerService, never()).save(any(Customer.class));
    }

    @Test
    public void shouldNotCreateCustomerWhenCPFIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("create-customer-null-cpf.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'CPF' cannot be null"));

        verify(customerService, never()).save(any(Customer.class));
    }

    @Test
    public void shouldNotCreateCustomerWhenBirthdateIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("create-customer-null-birthdate.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'Birthdate' cannot be null"));

        verify(customerService, never()).save(any(Customer.class));
    }

    @Test
    public void shouldNotCreateCustomerWhenCPFIsInvalid() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/customer")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("create-customer-invalid-cpf.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("CPF must contain only numbers and have maximum length of 11 chars"));

        verify(customerService, never()).save(any(Customer.class));
    }

    @Test
    public void shouldUpdateCustomer() throws Exception {
        when(customerService.update(anyLong(), any(Customer.class))).thenReturn(CustomerFixture.defaultValues());

        mockMvc.perform(MockMvcRequestBuilders
                .put("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("update-customer.json")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Customer 1"))
                .andExpect(jsonPath("$.cpf").value("11122233345"))
                .andExpect(jsonPath("$.birthdate").value("10-12-1990"))
                .andExpect(jsonPath("$.age").exists());

        verify(customerService, times(1)).update(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldNotUpdateCustomerWhenNameIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("update-customer-null-name.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'Name' cannot be null"));

        verify(customerService, never()).update(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldNotUpdateCustomerWhenCPFIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("update-customer-null-cpf.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'CPF' cannot be null"));

        verify(customerService, never()).update(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldNotUpdateCustomerWhenBirthdateIsNull() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("update-customer-null-birthdate.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("The field 'Birthdate' cannot be null"));

        verify(customerService, never()).update(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldNotUpdateCustomerWhenCPFIsInvalid() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("update-customer-invalid-cpf.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("CPF must contain only numbers and have maximum length of 11 chars"));

        verify(customerService, never()).update(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldPatchCustomerAndUpdateName() throws Exception {
        when(customerService.patch(anyLong(), any(Customer.class))).thenReturn(CustomerFixture.defaultValues());

        mockMvc.perform(MockMvcRequestBuilders
                .patch("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("patch-customer.json")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Customer 1"))
                .andExpect(jsonPath("$.cpf").value("11122233345"))
                .andExpect(jsonPath("$.birthdate").value("10-12-1990"))
                .andExpect(jsonPath("$.age").exists());

        verify(customerService, times(1)).patch(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldNotPatchCustomerWhenCPFIsInvalid() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .patch("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("patch-customer-invalid-cpf.json")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.message").value("CPF must contain only numbers and have maximum length of 11 chars"));

        verify(customerService, never()).patch(anyLong(), any(Customer.class));
    }

    @Test
    public void shouldDeleteCustomer() throws Exception {
        doNothing().when(customerService).delete(anyLong());

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/customer/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(fromFile("patch-customer-invalid-cpf.json")))
                .andExpect(status().isAccepted());

        verify(customerService, times(1)).delete(anyLong());
    }

    @SneakyThrows
    private byte[] fromFile(String path) {
        return StreamUtils.copyToByteArray(new ClassPathResource(path).getInputStream());
    }
}
