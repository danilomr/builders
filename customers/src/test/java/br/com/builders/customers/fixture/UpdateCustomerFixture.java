package br.com.builders.customers.fixture;

import br.com.builders.customers.controller.dto.UpdateCustomerDTO;

public class UpdateCustomerFixture {

    public static UpdateCustomerDTO defaultValues() {
        return UpdateCustomerDTO.builder()
                .name("Customer 1")
                .cpf("11122233345")
                .birthdate("10-10-2010")
                .build();
    }
}
