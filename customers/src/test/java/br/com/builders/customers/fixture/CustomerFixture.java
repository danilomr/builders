package br.com.builders.customers.fixture;

import br.com.builders.customers.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class CustomerFixture {

    public static Page<Customer> defaultPage() {
        return new PageImpl<>(defaultList());
    }

    public static List<Customer> defaultList() {
        return Arrays.asList(
                Customer.builder()
                        .id(1L)
                        .name("Customer 1")
                        .cpf("11122233345")
                        .birthdate(LocalDate.of(1990, 12, 10))
                        .build(),
                Customer.builder()
                        .id(2L)
                        .name("Customer 2")
                        .cpf("55544433312")
                        .birthdate(LocalDate.of(1990, 12, 10))
                        .build()
        );
    }

    public static Customer defaultValues() {
        return Customer.builder()
                .id(1L)
                .name("Customer 1")
                .cpf("11122233345")
                .birthdate(LocalDate.of(1990, 12, 10))
                .build();
    }
}
