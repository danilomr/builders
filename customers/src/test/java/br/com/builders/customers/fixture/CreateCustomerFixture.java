package br.com.builders.customers.fixture;

import br.com.builders.customers.controller.dto.CreateCustomerDTO;

public class CreateCustomerFixture {

    public static CreateCustomerDTO defaultValues() {
        return CreateCustomerDTO.builder()
                .name("Customer 1")
                .cpf("11122233345")
                .birthdate("10-10-2010")
                .build();
    }
}
