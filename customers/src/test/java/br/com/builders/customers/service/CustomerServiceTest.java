package br.com.builders.customers.service;

import br.com.builders.customers.entity.Customer;
import br.com.builders.customers.exception.CustomerAlreadyExistsException;
import br.com.builders.customers.exception.CustomerNotFoundException;
import br.com.builders.customers.fixture.CustomerFixture;
import br.com.builders.customers.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @InjectMocks
    private CustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    public void shouldFindCustomersWithNameAndCPF() {
        when(customerRepository.findByNameIgnoreCaseContainingAndCpf(anyString(), anyString(), any(Pageable.class)))
                .thenReturn(CustomerFixture.defaultPage());

        Page<Customer> customerPage = customerService.findByNameAndCpf("Customer", "11122233345", 0, 10);
        assertNotNull(customerPage);
        assertTrue(customerPage.getContent().size() == 2);

        verify(customerRepository, times(1))
                .findByNameIgnoreCaseContainingAndCpf(anyString(), anyString(), any(Pageable.class));
        verify(customerRepository, never()).findByNameIgnoreCaseContaining(anyString(), any(Pageable.class));
        verify(customerRepository, never()).findByCpf(anyString(), any(Pageable.class));
    }

    @Test
    public void shouldFindCustomersWithName() {
        when(customerRepository.findByNameIgnoreCaseContaining(anyString(), any(Pageable.class)))
                .thenReturn(CustomerFixture.defaultPage());

        Page<Customer> customerPage = customerService.findByNameAndCpf("Customer", null, 0, 10);
        assertNotNull(customerPage);
        assertTrue(customerPage.getContent().size() == 2);

        verify(customerRepository, times(1))
                .findByNameIgnoreCaseContaining(anyString(), any(Pageable.class));
        verify(customerRepository, never())
                .findByNameIgnoreCaseContainingAndCpf(anyString(), anyString(), any(Pageable.class));
        verify(customerRepository, never()).findByCpf(anyString(), any(Pageable.class));
    }

    @Test
    public void shouldFindCustomersWithCPF() {
        when(customerRepository.findByCpf(anyString(), any(Pageable.class)))
                .thenReturn(CustomerFixture.defaultPage());

        Page<Customer> customerPage = customerService.findByNameAndCpf(null, "11122233345", 0, 10);
        assertNotNull(customerPage);
        assertTrue(customerPage.getContent().size() == 2);

        verify(customerRepository, times(1)).findByCpf(anyString(), any(Pageable.class));
        verify(customerRepository, never())
                .findByNameIgnoreCaseContaining(anyString(), any(Pageable.class));
        verify(customerRepository, never())
                .findByNameIgnoreCaseContainingAndCpf(anyString(), anyString(), any(Pageable.class));
    }

    @Test
    public void shouldReturnEmptyPageWhenNameAndCPFAreNull() {
        Page<Customer> customerPage = customerService.findByNameAndCpf(null, null, 0, 10);
        assertNotNull(customerPage);
        assertTrue(customerPage.getContent().size() == 0);

        verify(customerRepository, never()).findByCpf(anyString(), any(Pageable.class));
        verify(customerRepository, never())
                .findByNameIgnoreCaseContaining(anyString(), any(Pageable.class));
        verify(customerRepository, never())
                .findByNameIgnoreCaseContainingAndCpf(anyString(), anyString(), any(Pageable.class));
    }

    @Test
    public void shouldSaveCustomer() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.existsByCpf(anyString())).thenReturn(false);
        when(customerRepository.save(any(Customer.class))).thenReturn(mockedCustomer);

        customerService.save(mockedCustomer);

        verify(customerRepository, times(1)).existsByCpf(anyString());
        verify(customerRepository, times(1)).save(any(Customer.class));
    }

    @Test
    public void shouldNotSaveAnExistingCustomer() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.existsByCpf(anyString())).thenReturn(true);
        try {
            customerService.save(mockedCustomer);
            fail();
        } catch (CustomerAlreadyExistsException ex) {
            assertEquals("Customer already exists with CPF [11122233345]", ex.getMessage());
        } catch (Exception ex) {
            fail();
        }

        verify(customerRepository, times(1)).existsByCpf(anyString());
        verify(customerRepository, never()).save(any(Customer.class));
    }

    @Test
    public void shouldUpdateCustomer() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(mockedCustomer));
        when(customerRepository.save(any(Customer.class))).thenReturn(mockedCustomer);

        customerService.update(1L, mockedCustomer);

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, times(1)).save(any(Customer.class));
    }

    @Test
    public void shouldNotUpdateCustomerWhenItsNotFound() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            customerService.update(1L, mockedCustomer);
            fail();
        } catch (CustomerNotFoundException ex) {
            assertEquals("Customer not found with id [1]", ex.getMessage());
        } catch (Exception ex) {
            fail();
        }

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, never()).save(any(Customer.class));
    }

    @Test
    public void shouldDeleteCustomer() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(mockedCustomer));
        doNothing().when(customerRepository).deleteById(anyLong());

        customerService.delete(1L);

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void shouldNotDeleteCustomerWhenItsNotFound() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            customerService.delete(1L);
            fail();
        } catch (CustomerNotFoundException ex) {
            assertEquals("Customer not found with id [1]", ex.getMessage());
        } catch (Exception ex) {
            fail();
        }

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, never()).deleteById(anyLong());
    }

    @Test
    public void shouldPatchCustomer() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(mockedCustomer));
        when(customerRepository.save(any(Customer.class))).thenReturn(mockedCustomer);

        customerService.patch(1L, mockedCustomer);

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, times(1)).save(any(Customer.class));
    }

    @Test
    public void shouldNotPatchCustomerWhenItsNotFound() {
        Customer mockedCustomer = CustomerFixture.defaultValues();
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            customerService.patch(1L, mockedCustomer);
            fail();
        } catch (CustomerNotFoundException ex) {
            assertEquals("Customer not found with id [1]", ex.getMessage());
        } catch (Exception ex) {
            fail();
        }

        verify(customerRepository, times(1)).findById(anyLong());
        verify(customerRepository, never()).save(any(Customer.class));
    }
}
