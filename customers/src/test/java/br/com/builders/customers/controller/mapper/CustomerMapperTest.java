package br.com.builders.customers.controller.mapper;

import br.com.builders.customers.controller.dto.*;
import br.com.builders.customers.entity.Customer;
import br.com.builders.customers.fixture.CreateCustomerFixture;
import br.com.builders.customers.fixture.CustomerFixture;
import br.com.builders.customers.fixture.PatchCustomerFixture;
import br.com.builders.customers.fixture.UpdateCustomerFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerMapperTest {

    @Test
    public void shouldMapEntityToDTO() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        Customer customer = CustomerFixture.defaultValues();
        CustomerDTO customerDTO = CustomerMapper.toDTO(customer);

        assertNotNull(customerDTO);
        assertEquals(customerDTO.getId(), customer.getId());
        assertEquals(customerDTO.getName(), customer.getName());
        assertEquals(customerDTO.getCpf(), customer.getCpf());
        assertEquals(customerDTO.getBirthdate(), formatter.format(customer.getBirthdate()));
        assertEquals(customerDTO.getAge(), customer.getAge());
    }

    @Test
    public void shouldMapCreateCustomerDTOToEntity() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        CreateCustomerDTO createCustomerDTO = CreateCustomerFixture.defaultValues();
        Customer customer = CustomerMapper.toEntity(createCustomerDTO);

        assertNotNull(customer);
        assertNull(customer.getId());
        assertEquals(customer.getName(), createCustomerDTO.getName());
        assertEquals(customer.getCpf(), createCustomerDTO.getCpf());
        assertEquals(customer.getBirthdate(), LocalDate.parse(createCustomerDTO.getBirthdate(), formatter));
    }

    @Test
    public void shouldMapUpdateCustomerDTOToEntity() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        UpdateCustomerDTO updateCustomerDTO = UpdateCustomerFixture.defaultValues();
        Customer customer = CustomerMapper.toEntity(updateCustomerDTO);

        assertNotNull(customer);
        assertNull(customer.getId());
        assertEquals(customer.getName(), updateCustomerDTO.getName());
        assertEquals(customer.getCpf(), updateCustomerDTO.getCpf());
        assertEquals(customer.getBirthdate(), LocalDate.parse(updateCustomerDTO.getBirthdate(), formatter));
    }

    @Test
    public void shouldMapPatchCustomerDTOToEntity() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        PatchCustomerDTO patchCustomerDTO = PatchCustomerFixture.defaultValues();
        Customer customer = CustomerMapper.toEntity(patchCustomerDTO);

        assertNotNull(customer);
        assertNull(customer.getId());
        assertEquals(customer.getName(), patchCustomerDTO.getName());
        assertEquals(customer.getCpf(), patchCustomerDTO.getCpf());
        assertEquals(customer.getBirthdate(), LocalDate.parse(patchCustomerDTO.getBirthdate(), formatter));
    }

    @Test
    public void shouldMapCustomerPageToGetCustomerDTO() {
        Page<Customer> customerPage = CustomerFixture.defaultPage();
        GetCustomerDTO getCustomerDTO = CustomerMapper.toDTOPage(customerPage);

        assertNotNull(getCustomerDTO);
        assertNotNull(getCustomerDTO.getContent());
        assertNotNull(getCustomerDTO.getPage());
        assertEquals(getCustomerDTO.getContent().size(), customerPage.getContent().size());
    }
}
