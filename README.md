# Projeto Customers #

O Projeto trata-se de um CRUD completo implementado conforme os requisitos do teste.

### Tecnologias ###

* Java
* Spring Boot
* Lombok
* Apache Commons
* MySQL
* Docker
* Mockito/JUnit

### Pré Requisitos ###

* Java
* Docker
* Postman
* Git
* Maven

### Como rodar ###

1- Após fazer o pull do projeto, acesse a pasta raiz pelo terminal (onde se encontra o arquivo docker-compose.yml) e execute o seguinte comando:

	docker pull mysql:5.7

2- Depois de finalizado o download da imagem do MySQL, iremos compilar o projeto e gerar a imagem da aplicação:

	mvn package && docker-compose up -d
	
3- Aguarde alguns segundos e a aplicação estará pronta para ser testada.

### Conclusão ###

Conforme solicitado, na raiz do projeto deixei uma collection do Postman (Customers.postman_collection.json), dessa forma é necessário apenas importá-la e nela 
estarão salvos todos os endpoints do CRUD.